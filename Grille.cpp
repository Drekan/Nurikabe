#include "Grille.h"
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

// Constructeurs
	Grille::Grille():ligne(10),colonne(10){
		DEBUG = testDebug();
	nbrCasesBlanchesGrille=0;
	tailleFinaleCaseNoire=0;

	grille = new Case*[colonne];
	for (int x = 0; x< colonne; x++){
		grille[x] = new Case[ligne];}

		for (int x = 0; x< ligne; x++ ){
			for (int y =0;y < colonne; y++){

				grille[x][y].setCoordCase(x,y);
			}
		}

        grille[0][0].changeNumero(2);
        grille[0][3].changeNumero(2);
        grille[0][5].changeNumero(3);
        grille[0][7].changeNumero(2);
        
        grille[2][0].changeNumero(1);
        grille[2][7].changeNumero(2);


        grille[3][2].changeNumero(2);
        grille[3][6].changeNumero(1);

        grille[4][0].changeNumero(3);
        grille[4][4].changeNumero(2);
        grille[4][8].changeNumero(1);

        grille[5][3].changeNumero(2);
        grille[5][9].changeNumero(2);

        grille[6][7].changeNumero(3);

        grille[7][1].changeNumero(2);
        grille[7][4].changeNumero(2);
        grille[7][6].changeNumero(1);

        grille[8][8].changeNumero(1);

        grille[9][0].changeNumero(2);
        grille[9][7].changeNumero(5);
		for(int i=0;i<ligne;i++){
			for(int j=0;j<colonne;j++){
				if(grille[i][j].getNumero()>0){
					nbrCasesBlanchesGrille+=grille[i][j].getNumero();
				}
			}
		}

		tailleFinaleCaseNoire=(ligne*colonne)-nbrCasesBlanchesGrille;
	}

Grille::Grille(Case** grilleCase,int l,int c) : ligne(l), colonne(c){
	DEBUG = testDebug();
	nbrCasesBlanchesGrille=0;
	tailleFinaleCaseNoire=0;
	grille=copierGrilleCase(grilleCase);
	for(int i=0;i<ligne;i++){
		for(int j=0;j<colonne;j++){
			if(grille[i][j].getNumero()>0){
				nbrCasesBlanchesGrille+=grille[i][j].getNumero();
			}
		}
	}

	tailleFinaleCaseNoire=(ligne*colonne)-nbrCasesBlanchesGrille;

}

	/*Grille::~Grille(){
		for(int i=0;i<ligne;i++){
			delete[] grille[i];
		}
		delete grille;
	}*/


/* près-requis : OK-savoir le nombre de grille dans le fichier
				 OK-obtenir le format de la grille située sur la ieme ligne (ligne x colonne)
				 OK-récupérer un Case** à partir de ces infos (i x ligne x colonne)
				 OK-Descripteur de fichier pour sélectionner une grille
			gdb:
			g++ -g -Wall -c -std=c++14 Grille.cpp case.cpp outils-grille.cpp Coord.cpp
			g++ -g -Wall -std=c++14 test-main.cpp outils-grille.o case.o Grille.o Coord.o -o test
*/
	Grille::Grille(std::string nomFichier){
		DEBUG = testDebug();
		int nbrGrille=getNbrGrillesDisponibles(nomFichier);
		cout<<descripteurGrilles(nomFichier);
		cout<<"Quelle grille voulez-vous charger ?\n";
		int choix_utilisateur;
		do{
			cout<<">> ";
			cin>>choix_utilisateur;
		}while(choix_utilisateur<1 || choix_utilisateur>nbrGrille);
		choix_utilisateur--;

		ligne=getFormatGrille_ligne(nomFichier,choix_utilisateur);
		colonne=getFormatGrille_colonne(nomFichier,choix_utilisateur);
		//cout<<"ligne :"<<ligne<<"   colonne:"<<colonne<<endl;
		grille=chargerGrilleFichier(nomFichier,choix_utilisateur);

		nbrCasesBlanchesGrille=0;
		tailleFinaleCaseNoire=0;
		for(int i=0;i<ligne;i++){
			for(int j=0;j<colonne;j++){
				if(grille[i][j].getNumero()>0){
					nbrCasesBlanchesGrille+=grille[i][j].getNumero();
					grille[i][j].changeNumero(grille[i][j].getNumero());
				}
			}
		}

		tailleFinaleCaseNoire=(ligne*colonne)-nbrCasesBlanchesGrille;
	}
int Grille::getNbrGrillesDisponibles(std::string nomFichier){
	int nbrGrille=0;
	ifstream fichier;
	fichier.open(nomFichier);

	char lecteurFichier;
	do{
		lecteurFichier=fichier.get();
		if(lecteurFichier=='\n'){
			nbrGrille++;
		}
	}while(lecteurFichier!=EOF);
	fichier.close();
	return nbrGrille;
}

int Grille::getFormatGrille_ligne(std::string nomFichier,int i){
	int nbrGrille=getNbrGrillesDisponibles(nomFichier);
	int nbrSautDeLigne=nbrGrille-(nbrGrille-i);

	//positionnement du curseur sur la bonne ligne
	ifstream fichier;
	fichier.open(nomFichier);
	char lecteurFichier;
	if(i>0){
		do{
			lecteurFichier=fichier.get();
			if(lecteurFichier=='\n'){
				nbrSautDeLigne--;
			}
		}while(lecteurFichier!=EOF && nbrSautDeLigne>0);
	}

	int nbrLigne=1;
	//lecture du nombre de lignes
	do{
		lecteurFichier=fichier.get();
		if(lecteurFichier==':'){
			nbrLigne++;
		}
	}while(lecteurFichier!='\n' && lecteurFichier!=EOF);
	fichier.close();
	return nbrLigne;

}

int Grille::getFormatGrille_colonne(std::string nomFichier,int i){
	int nbrGrille=getNbrGrillesDisponibles(nomFichier);
	int nbrSautDeLigne=nbrGrille-(nbrGrille-i);

	//positionnement du curseur sur la bonne ligne
	ifstream fichier;
	fichier.open(nomFichier);
	char lecteurFichier;
	if(i>0){
		do{
			lecteurFichier=fichier.get();
			if(lecteurFichier=='\n'){
				nbrSautDeLigne--;
			}
		}while(lecteurFichier!=EOF && nbrSautDeLigne>0);
	}
	int nbrColonne=0;
	bool lectureNumero=false;
	do{
		lecteurFichier=fichier.get();
		if(lecteurFichier!=':'){
			if(lecteurFichier=='N' || lecteurFichier=='B'){
				nbrColonne++;
				lectureNumero=false;
			}
			else if(!lectureNumero){
				lectureNumero=true;
				nbrColonne++;
			}
		}
	}while(lecteurFichier!=':' && lecteurFichier!=EOF);

	fichier.close();
	return nbrColonne;
}

Case** Grille::chargerGrilleFichier(std::string nomFichier,int numeroGrille){
	int nbrGrille=getNbrGrillesDisponibles(nomFichier);
	int numGrille=numeroGrille%nbrGrille;
	int nbrLigne=getFormatGrille_ligne(nomFichier,numeroGrille);
	int nbrColonne=getFormatGrille_colonne(nomFichier,numeroGrille);
	int nbrSautDeLigne=nbrGrille-(nbrGrille-numGrille);


	Case** grilleCase=new Case*[nbrLigne];

	for(int i=0;i<nbrLigne;i++){
		grilleCase[i]=new Case[nbrColonne];
	}

	for(int i=0;i<nbrLigne;i++){
		for(int j=0;j<nbrColonne;j++){
			grilleCase[i][j]=Case();
		}
	}

	ifstream fichier;
	fichier.open(nomFichier);
	char lecteurFichier;
	if(numGrille>0){
		do{
			lecteurFichier=fichier.get();
			if(lecteurFichier=='\n'){
				nbrSautDeLigne--;
			}
		}while(lecteurFichier!=EOF && nbrSautDeLigne>0);
	}
	int i=0;
	int j=0;
	//int puissance10=0;
	bool lectureNumero=false;
	int numeroProvisoire=0;
	do{
		lecteurFichier=fichier.get();
		if(lecteurFichier=='N'){
			if(lectureNumero){
				lectureNumero=false;
				grilleCase[i][j].changeNumero(numeroProvisoire);
				numeroProvisoire=0;
				//grilleCase[i][j].setCouleur(Case::Couleur::BLANCHE);
				grilleCase[i][j].setCoordCase(i,j);
				//grilleCase[i][j].setCoordCaseGroupe(i,j);
				j++;
				if(j==nbrColonne){
					j=0;
					i++;
				}
			}
			grilleCase[i][j].setCouleur(Case::Couleur::GRISE);
			grilleCase[i][j].setCoordCase(i,j);
			
			j++;
			if(j==nbrColonne){
				j=0;
				i++;
			}
		}
		else if(lecteurFichier=='B'){
			if(lectureNumero){
				lectureNumero=false;
				grilleCase[i][j].changeNumero(numeroProvisoire);
				numeroProvisoire=0;
				//grilleCase[i][j].setCouleur(Case::Couleur::BLANCHE);
				grilleCase[i][j].setCoordCase(i,j);
				//grilleCase[i][j].setCoordCaseGroupe(i,j);
				j++;
				if(j==nbrColonne){
					j=0;
					i++;
				}
			}
			grilleCase[i][j].setCouleur(Case::Couleur::GRISE);
			grilleCase[i][j].setCoordCase(i,j);
			j++;
			if(j==nbrColonne){
				j=0;
				i++;
			}
		}
		else if(lecteurFichier!=':' && lecteurFichier!='\n' && lecteurFichier!=EOF){
			lectureNumero=true;
			numeroProvisoire*=10;
			//cout<<numeroProvisoire<<" -> ";
			numeroProvisoire+=lecteurFichier-'0';
			//cout<<numeroProvisoire<<endl;
			
		}
		else{
			if(lectureNumero){
				lectureNumero=false;
				grilleCase[i][j].changeNumero(numeroProvisoire);
				numeroProvisoire=0;
				//grilleCase[i][j].setCouleur(Case::Couleur::BLANCHE);
				grilleCase[i][j].setCoordCase(i,j);
				//grilleCase[i][j].setCoordCaseGroupe(i,j);
				j++;
				if(j==nbrColonne){
					j=0;
					i++;
				}
			}
		}


	}while(lecteurFichier!=EOF && lecteurFichier!='\n');
	fichier.close();
	return grilleCase;
}


std::string Grille::descripteurGrilles(std::string nomFichier){
	int nbrGrille=getNbrGrillesDisponibles(nomFichier);
	std::string description=std::to_string(nbrGrille);
	description+=" grille(s) disponible(s)\n----------------\n(format : ligne x colonne)\n";
	for(int i=0;i<nbrGrille;i++){
		description+=std::to_string(i+1);
		description+=": (";
		description+=std::to_string(getFormatGrille_ligne(nomFichier,i));
		description+="l x ";
		description+=std::to_string(getFormatGrille_colonne(nomFichier,i));
		description+="c )\n";
	}
	return description;
}



	Grille::Grille(Grille& g) : ligne(g.getLigne()),colonne(g.getColonne()){

		DEBUG=false;
		this->nbrCasesBlanchesGrille=g.nbrCasesBlanchesGrille;
		this->tailleFinaleCaseNoire=g.tailleFinaleCaseNoire;
		this->grille=new Case*[ligne];
		for(int i=0;i<ligne;i++){
			this->grille[i]=new Case[colonne];
		}

		for(int i=0;i<ligne;i++){
			for(int j=0;j<colonne;j++){
				this->grille[i][j]=Case(g.grille[i][j]);
			}
		}
	}
	
// Accesseurs
	const int Grille::getLigne(){return ligne; }
	const int Grille::getColonne() {return colonne; }
	Case** Grille::getGrille(){
		return grille;
	}
int Grille::nbrCaseProchesGrises_V1(int x, int y){
	int compteur = 0;
	if (estDansGrille(x,y-1)){ if (estGrise(x, y-1)) compteur++;}
	if (estDansGrille(x,y+1)){ if (estGrise(x, y+1)) compteur++;}
	if (estDansGrille(x-1,y)){ if (estGrise(x-1, y)) compteur++;}
	if (estDansGrille(x+1,y)){ if (estGrise(x+1,y)) compteur++;}
	return compteur;
}
//// Test Bool
bool Grille::estBlanche(int x, int y) {
	return grille[x][y].getCouleur() == Case::Couleur::BLANCHE;
}

bool Grille::estNoire(int x, int y) {
	return grille[x][y].getCouleur() == Case::Couleur::NOIRE;
}

bool Grille::estGrise(int x, int y) {
	return grille[x][y].getCouleur() == Case::Couleur::GRISE;
}

bool Grille::estDansGrille(int x, int y){
	return (x>=0 && x<ligne && y>=0 && y<ligne);
}

void Grille::quelCouleurTerminal(int x, int y){
	cout << "La case [" << x << "] [" << y << "] est ";
	if (grille[x][y].getCouleur() == Case::Couleur::BLANCHE) cout << " Blanche " << endl ;
	if (grille[x][y].getCouleur() == Case::Couleur::NOIRE) cout << " Noire " << endl ;
	if (grille[x][y].getCouleur() == Case::Couleur::GRISE) cout << " Grise " << endl ;
}

void Grille::casUn(int x, int y){
	if(grille[x][y].getNumero() == 1){
		if(estDansGrille(x+1,y)){
				devientNoire(x+1,y);
		}

		if(estDansGrille(x-1,y)){
				devientNoire(x-1,y);
		}

		if(estDansGrille(x,y+1)){
				devientNoire(x,y+1);
		}

		if(estDansGrille(x,y-1)){
				devientNoire(x,y-1);
		}
	}
}

void Grille::rechercheCaseNonAtteignable(int x, int y, int d, bool** grilleBool){
	if(d != 0){
		grilleBool[x][y] = true;

		if(estDansGrille(x+1,y)){// && grilleBool[x+1][y] != true){
			rechercheCaseNonAtteignable(x+1,y,d-1,grilleBool);
		}

		if(estDansGrille(x-1,y)){// && grilleBool[x-1][y] != true){
			rechercheCaseNonAtteignable(x-1,y,d-1,grilleBool);
		}

		if(estDansGrille(x,y+1)){// && grilleBool[x][y+1] != true){
			rechercheCaseNonAtteignable(x,y+1,d-1,grilleBool);
		}

		if(estDansGrille(x,y-1)){// && grilleBool[x][y-1] != true){
			rechercheCaseNonAtteignable(x,y-1,d-1,grilleBool);
		}
	}
}

void Grille::premierParcours(){
	//Création de la grille de booléens
	bool** grilleBool = new bool*[colonne];
	for(int i = 0; i < colonne; i++){
		grilleBool[i] = new bool[ligne];
	}

	//Initialisations de la grille de booléens à false
	for(int i = 0; i < colonne; i++){
		for(int j = 0; j < ligne; j++){
			grilleBool[i][j] = false; // pour debug autre chose que rechercheCaseNonAtteignable, passer à True, remettre à false après
		}
	}

	for(int i = 0; i < colonne; i++){
		for(int j = 0; j < ligne; j++){
			if(grille[i][j].getNumero() > 0){
				casUn(i,j);
				rechercheCaseNonAtteignable(i,j,grille[i][j].getNumero(),grilleBool);
				rechercheEntreDeux(i,j);
			}
		}
	}

	for(int i = 0; i < colonne; i++){
		for(int j = 0; j < ligne; j++){
			if(grilleBool[i][j] == false){
				devientNoire(i,j);
			}
		}
	}
	for(int i=0;i<colonne;i++){
		delete[] grilleBool[i];
	}
	delete[] grilleBool;
}
bool lever=false;
void Grille::rechercheEntreDeux(int x, int y) {
	
	// Cas dans l'axe
	if (estDansGrille(x+2,y) && grille[x+2][y].getNumero()>0){

			devientNoire(x+1,y);
	}
	if (estDansGrille(x-2,y) && grille[x-2][y].getNumero()>0){
			devientNoire(x-1,y);
	}
//if(!lever && estNoire(9,5)){cout<<"x: "<<x<<" , y: "<<y<<endl;lever=true;}
	if (estDansGrille(x,y+2) && grille[x][y+2].getNumero()>0){
		devientNoire(x,y+1);
	}
//if(!lever && estNoire(9,5)){cout<<"x: "<<x<<" , y: "<<y<<endl;lever=true;}
	if (estDansGrille(x,y-2) && grille[x][y-2].getNumero()>0){
			devientNoire(x,y-1);
	}

	// Cas en diagonale
	if (estDansGrille(x-1,y-1) && grille[x-1][y-1].getNumero()>0){
			devientNoire(x-1,y);
			devientNoire(x,y-1);
	}
	if (estDansGrille(x-1,y+1) && grille[x-1][y+1].getNumero()>0){
		devientNoire(x-1,y);
		devientNoire(x,y+1);
	}
	if (estDansGrille(x+1,y-1) && grille[x+1][y-1].getNumero()>0){
			devientNoire(x+1,y);
			devientNoire(x,y-1);
	}
	if (estDansGrille(x+1,y+1) && grille[x+1][y+1].getNumero()>0){
		devientNoire(x+1,y);
		devientNoire(x,y+1);
	}
/*if(!lever && estNoire(9,5)){
			cout<<"x: "<<x<<" , y: "<<y<<endl;
			lever=true;
		}*/
}


bool Grille::entoureParBlancOuVide(int x , int y){// renvoie true si la case est entourés de blancs ou de vide, sauf une case


	int cmpt=0;
	if(!estDansGrille(x+1,y) || estBlanche(x+1,y)){
		cmpt++;
	}
	if(!estDansGrille(x-1,y) || estBlanche(x-1,y)){
		cmpt++;
	}
	if(!estDansGrille(x,y+1) || estBlanche(x,y+1)){
		cmpt++;
	}
	if(!estDansGrille(x,y-1) || estBlanche(x,y-1)){
		cmpt++;
	}

	return cmpt==3;
}

void Grille::isolementCaseNoire(int x, int y){
	if(entoureParBlancOuVide(x,y)){
		if(estDansGrille(x+1,y) && estGrise(x+1,y))
		devientNoire(x+1,y);

		if(estDansGrille(x,y+1) && estGrise(x,y+1))
		devientNoire(x,y+1);

		if(estDansGrille(x-1,y) && estGrise(x-1,y))
		devientNoire(x-1,y);

		if(estDansGrille(x,y-1) && estGrise(x,y-1))
		devientNoire(x,y-1);
	}
}


bool Grille::entoureParNoirOuVide(int x , int y){// renvoie true si la case est entourés de blancs ou de vide, sauf une case

	int cmpt=0;
	if(!estDansGrille(x+1,y) || estNoire(x+1,y)){
		cmpt++;
	}
	//else if (estDansGrille(x+1,y) && estGrise(x+1,y) && blancOuVide(x+1,y)) cmpt++;

	if(!estDansGrille(x-1,y) || estNoire(x-1,y)){
		cmpt++;
	}
	//else if (estDansGrille(x-1,y) && estGrise(x-1,y) && blancOuVide(x-1,y)) cmpt++;

	if(!estDansGrille(x,y+1) || estNoire(x,y+1)){
		cmpt++;
	}
	//else if (estDansGrille(x,y+1) && estGrise(x,y+1) && blancOuVide(x,y+1)) cmpt++;

	if(!estDansGrille(x,y-1) || estNoire(x,y-1)){
		cmpt++;
	}
	//else if (estDansGrille(x,y-1) && estGrise(x,y-1) && blancOuVide(x,y-1)) cmpt++;

	return cmpt==3;
}

void Grille::isolementCaseBlanche(int x, int y){
	if(entoureParNoirOuVide(x,y)){
		if(estDansGrille(x+1,y) && estGrise(x+1,y))
		devientBlanche(x+1,y);
		if(estDansGrille(x,y+1) && estGrise(x,y+1))
		devientBlanche(x,y+1);

		if(estDansGrille(x-1,y) && estGrise(x-1,y))
		devientBlanche(x-1,y);

		if(estDansGrille(x,y-1) && estGrise(x,y-1))
		devientBlanche(x,y-1);
	}

}


//prend les coord d'une case grise, le test a été fait
void Grille::carreNoir(int x,int y){
	//zone 1
	if(estDansGrille(x-1,y) && estNoire(x-1,y)){
		if(estDansGrille(x-1,y-1) && estNoire(x-1,y-1)){
			if(estDansGrille(x,y-1) && estNoire(x,y-1)){
				devientBlanche(x,y);
			}
		}
	}
	//zone 2
	if(estDansGrille(x+1,y) && estNoire(x+1,y)){
		if(estDansGrille(x,y-1) && estNoire(x,y-1)){
			if(estDansGrille(x+1,y-1) && estNoire(x+1,y-1)){
				devientBlanche(x,y);
			}
		}
	}
	// 3
	if(estDansGrille(x,y+1) && estNoire(x,y+1)){
		if(estDansGrille(x+1,y) && estNoire(x+1,y)){
			if(estDansGrille(x+1,y+1) && estNoire(x+1,y+1)){
				devientBlanche(x,y);
			}
		}
	}
	// quatre
	if(estDansGrille(x,y+1) && estNoire(x,y+1)){
		if(estDansGrille(x-1,y) && estNoire(x-1,y)){
			if(estDansGrille(x-1,y+1) && estNoire(x-1,y+1)){
				devientBlanche(x,y);
			}
		}
	}
}



void Grille::afficheDEBUG(){
	for (int x = 0; x< ligne; x++ ){
		for (int y =0;y < colonne; y++){
			if (grille[x][y].getNumero() > 0) cout << " " << grille[x][y].getNumero() << " ";
			else if (estBlanche(x,y)) cout << " B ";
			else if (estNoire(x,y)) cout << " N ";
			else if (estGrise(x,y)) cout << " G ";
		}
		cout << endl;
	}
}

int Grille::nbChiffre(int nombre){
	int nb=1;
	int copieNombre=nombre;
	while(copieNombre>9){
		copieNombre/=10;
		nb++;
	}
	return nb;
}

int Grille::getChiffreNombre(int nombre, int chiffre){
	int retour;
	if(chiffre>=nbChiffre(nombre)){
		retour=nombre%10;
	}
	else{
		int copie=nombre;
		for(int i=chiffre;i>0;i--){
			copie/=10;
		}
		retour=copie%10;
	}
	return retour;
}

int Grille::getTailleImpression(){
	int tailleInitiale=colonne*ligne+ligne;
	int ajout=0;
	for(int i=0;i<ligne;i++){
		for(int j=0;j<colonne;j++){
			if(grille[i][j].getNumero()!=-1){
				ajout+=nbChiffre(grille[i][j].getNumero())-1;
			}
		}
	}
	return tailleInitiale+ajout;
}
void Grille::imprimerGrille(char const * nomFichier){

	int nbrColonne=this->colonne;
	int nbrLigne=this->ligne;

	int tailleTab=getTailleImpression();
	char * tableauEcriture=new char[tailleTab];
	int indiceEcriture=0;
	for(int i=0;i<nbrLigne;i++)
	{
		if(i!=0){
			tableauEcriture[indiceEcriture]=':';
			indiceEcriture++;
		}
		for(int j=0;j<nbrColonne;j++)
		{
			if(estBlanche(i,j))
			{
				if(grille[i][j].getNumero()==-1){
					tableauEcriture[indiceEcriture]='B';
				}
				else{
					//tableauEcriture[indiceEcriture]=grille[i][j].getNumero();
					for(int c=nbChiffre(grille[i][j].getNumero())-1;c>=0;c--){
						//cout<<c<<endl;//getChiffreNombre(grille[i][j].getNumero(),c)<<endl;
						tableauEcriture[indiceEcriture]=getChiffreNombre(grille[i][j].getNumero(),c)+'0';
						indiceEcriture++;
					}
					indiceEcriture--;
				}
			}
			else if(estNoire(i,j)){
				tableauEcriture[indiceEcriture]='N';
			}
			else if(estGrise(i,j)){
				tableauEcriture[indiceEcriture]='?';
			}

			indiceEcriture++;
		}
	}
	tableauEcriture[indiceEcriture]='\n';

	ofstream fichier;
	fichier.open(nomFichier,ios_base::out);
	fichier.write(tableauEcriture,tailleTab);
	fichier.close();
	delete[] tableauEcriture;
}

void Grille::marquerCasesPotentielles(bool ** grilleBool,Case& c){
	int x=c.getCoordX();
	int y=c.getCoordY();
	grilleBool[x][y]=true;

	if(estDansGrille(x+1,y) && !grilleBool[x+1][y]){
		if(estNoire(x+1,y)){
			marquerCasesPotentielles(grilleBool, grille[x+1][y]);
		}
		else if(estGrise(x+1,y)){
			grilleBool[x+1][y]=true;
		}
	}

	if(estDansGrille(x-1,y) && !grilleBool[x-1][y]){
		if(estNoire(x-1,y)){
			marquerCasesPotentielles(grilleBool,grille[x-1][y]);
		}
		else if(estGrise(x-1,y)){
			grilleBool[x-1][y]=true;
		}
	}

	if(estDansGrille(x,y+1) && !grilleBool[x][y+1]){
		if(estNoire(x,y+1)){
			marquerCasesPotentielles(grilleBool,grille[x][y+1]);
		}
		else if(estGrise(x,y+1)){
			grilleBool[x][y+1]=true;
		}
	}

	if(estDansGrille(x,y-1) && !grilleBool[x][y-1]){
		if(estNoire(x,y-1)){
			marquerCasesPotentielles(grilleBool,grille[x][y-1]);
		}
		else if(estGrise(x,y-1)){
			grilleBool[x][y-1]=true;
		}
	}
}

void Grille::etendreLigneNoire(Case& c){
	bool ** grilleBool=new bool*[ligne];
	for(int i=0;i<ligne;i++){
		grilleBool[i]=new bool[colonne];
	}

	for(int i=0;i<ligne;i++){
		for(int j=0;j<colonne;j++){
			grilleBool[i][j]=false;
		}
	}

	marquerCasesPotentielles(grilleBool,c);

	int compteurCasePossibles=0;
	int coordCaseX=0;
	int coordCaseY=0;
	for(int i=0;i<ligne;i++){
		for(int j=0;j<colonne;j++){
			if(grilleBool[i][j] && estGrise(i,j)){
				compteurCasePossibles++;
				coordCaseX=i;
				coordCaseY=j;
			}
		}
	}
	if(compteurCasePossibles==1){
		if(viableCarreNoir(coordCaseX,coordCaseY))
			if(nbrCaseNoire()<tailleFinaleCaseNoire)
				devientNoire(coordCaseX,coordCaseY);
	}
	for(int i=0;i<ligne;i++){
		delete[] grilleBool[i];
	}
	delete[] grilleBool;
}

void Grille::devientNoire(int x, int y){
//if(!lever && estNoire(9,5)){cout<<"x: "<<x<<" , y: "<<y<<endl;lever=true;}

//DEBUG
if(DEBUG){
	if (estBlanche(x,y)) {cout << "ERREUR !!! La case [" << x << "] [ " << y << "] était Blanche" << endl; return;}
	else cout << "La case [" << x << "] [ " << y << "] devient Noire" << endl ;
}
//FIN DEBUG

	grille[x][y].changeCouleur(Case::Couleur::NOIRE);


//DEBUG 
if(DEBUG){
	afficheDEBUG(); 
	int DEBUG; 
	cin >> DEBUG; 
}
//FIN DEBUG

	int posX=x+1;
	int posY=y;
	if(estDansGrille(posX,posY)) testGeneraux(posX,posY);
//if(!lever && estNoire(9,5)){cout<<"x: "<<x<<" , y: "<<y<<endl;lever=true;}

	posX=x-1;
	posY=y;
	if(estDansGrille(posX,posY)) testGeneraux(posX,posY);

	posX=x;
	posY=y+1;
	if(estDansGrille(posX,posY)) testGeneraux(posX,posY);

	posX=x;
	posY=y-1;
	if(estDansGrille(posX,posY)) testGeneraux(posX,posY);
//if(!lever && estNoire(9,5)){cout<<"x: "<<x<<" , y: "<<y<<endl;lever=true;}
}

void Grille::devientBlanche(int x, int y){
//DEBUG
if(DEBUG){
	if (estNoire(x,y)) {cout << "ERREUR !!! La case [" << x << "] [ " << y << "] était Noire" << endl; return;}
	else cout << "La case [" << x << "] [ " << y << "] devient Blanche"<< endl ;
}
//FIN DEBUG



	//Modif case et recherche de son groupement
	grille[x][y].changeCouleur(Case::Couleur::BLANCHE);

//DEBUG
if(DEBUG){
	afficheDEBUG(); 
	int DEBUG; 
	cin >> DEBUG;
} 
//FIN DEBUG

	//cout << x <<  y << "Ok " << endl;
	chercheGroupement(x,y);
	//cout << "chercheGroupement" << endl;
	if (nbrCaseGroupement(x,y)==getNumeroGroupement(x,y)) contourNoir(x,y);

	int posX=x+1;
	int posY=y;
	if(estDansGrille(posX,posY)) testGeneraux(posX,posY);


	posX=x-1;
	posY=y;
	if(estDansGrille(posX,posY)) testGeneraux(posX,posY);

	posX=x;
	posY=y+1;
	if(estDansGrille(posX,posY)) testGeneraux(posX,posY);

	posX=x;
	posY=y-1;
	if(estDansGrille(posX,posY)) testGeneraux(posX,posY);
}

void Grille::parcoursV1(){
	int caseGriseDebut = nbrCaseGrise();

	for (int x = 0; x< ligne; x++ ){
		for (int y =0;y < colonne; y++){
			if (estNoire(x,y)){
				etendreLigneNoire(grille[x][y]);
			}
			else if (estGrise(x,y)){
			 carreNoir(x,y);
			}
			else{
			 	if (!groupementFini(x,y))etendreGroupement(grille[x][y]);
			 	if(groupementFini(x,y)){
			 		contourNoir(x,y);
			 	}
			 }
			if (estBlanche(x,y) && !groupementFini(x,y)) isolementCaseBlanche(x,y);
		}
	}
	if (caseGriseDebut != nbrCaseGrise()){
		//cout<< "New Parcours " << endl;
		parcoursV1();
	}
}

void Grille::chercheGroupement(int x, int y){
	int posX=x+1;
	int posY=y;

	if(estDansGrille(posX,posY) && grille[posX][posY].getCouleur()==Case::Couleur::BLANCHE){
		if(grille[posX][posY].getCoordCaseGroupement().getX() != -1){
			grille[x][y].setCoordCaseGroupement(grille[posX][posY]);
		}
	}

	posX=x-1;
	posY=y;

	if(estDansGrille(posX,posY) && grille[posX][posY].getCouleur()==Case::Couleur::BLANCHE){
		if(grille[posX][posY].getCoordCaseGroupement().getX() != -1){
			grille[x][y].setCoordCaseGroupement(grille[posX][posY]);
		}
	}

	posX=x;
	posY=y+1;

	if(estDansGrille(posX,posY) && grille[posX][posY].getCouleur()==Case::Couleur::BLANCHE){
		if(grille[posX][posY].getCoordCaseGroupement().getX() != -1){
			grille[x][y].setCoordCaseGroupement(grille[posX][posY]);
		}
	}

	posX=x;
	posY=y-1;

	if(estDansGrille(posX,posY) && grille[posX][posY].getCouleur()==Case::Couleur::BLANCHE){
		if(grille[posX][posY].getCoordCaseGroupement().getX() != -1){
			grille[x][y].setCoordCaseGroupement(grille[posX][posY]);
		}
	}

}

void Grille::marquerCaseGroupement(Case c,bool ** grilleBool){
	int x=c.getCoordX();
	int y=c.getCoordY();

	grilleBool[x][y]=true;

	int posX=x+1;
	int posY=y;

	if(estDansGrille(posX,posY)){
		if(grille[posX][posY].getCouleur()==Case::Couleur::BLANCHE && !grilleBool[posX][posY]){
			marquerCaseGroupement(grille[posX][posY],grilleBool);
		}
	}

	posX=x-1;
	posY=y;

	if(estDansGrille(posX,posY)){
		if(grille[posX][posY].getCouleur()==Case::Couleur::BLANCHE && !grilleBool[posX][posY]){
			marquerCaseGroupement(grille[posX][posY],grilleBool);
		}
	}

	posX=x;
	posY=y+1;

	if(estDansGrille(posX,posY)){
		if(grille[posX][posY].getCouleur()==Case::Couleur::BLANCHE && !grilleBool[posX][posY]){
			marquerCaseGroupement(grille[posX][posY],grilleBool);
		}
	}

	posX=x;
	posY=y-1;

	if(estDansGrille(posX,posY)){
		if(grille[posX][posY].getCouleur()==Case::Couleur::BLANCHE && !grilleBool[posX][posY]){
			marquerCaseGroupement(grille[posX][posY],grilleBool);
		}
	}
}


//algo experimental : prends une case Blanche et renvoie le nombre
//de case appartenant au groupement de celle-ci
int Grille::nbrCaseGroupement(int x, int y){
		int	compteurDeCase=-1;
	
	if(grille[x][y].getCouleur()==Case::Couleur::BLANCHE){
		bool ** grilleBool=new bool*[ligne];
		for(int i=0;i<ligne;i++){
			grilleBool[i]=new bool[colonne];
		}

		for(int i=0;i<ligne;i++){
			for(int j=0;j<colonne;j++){
				grilleBool[i][j]=false;
			}
		}

		marquerCaseGroupement(grille[x][y],grilleBool);

		compteurDeCase =0;
		for(int i=0;i<ligne;i++){
			for(int j=0;j<colonne;j++){
				if(grilleBool[i][j])
					compteurDeCase++;
			}
		}

		for(int i=0;i<ligne;i++){
			delete[] grilleBool[i];
		}
		delete[] grilleBool;
	}

	return compteurDeCase;
}

void Grille::chercherNumeroGroupement(Case c,int ** grilleInt){
	int x=c.getCoordX();
	int y=c.getCoordY();
	if(c.getNumero()==-1)
		grilleInt[x][y]=0;
	else
		grilleInt[x][y]=c.getNumero();

	int posX=x+1;
	int posY=y;

	if(estDansGrille(posX,posY)){
		if(grille[posX][posY].getCouleur()==Case::Couleur::BLANCHE && grilleInt[posX][posY]<0){
			chercherNumeroGroupement(grille[posX][posY],grilleInt);
		}
	}

	posX=x-1;
	posY=y;

	if(estDansGrille(posX,posY)){
		if(grille[posX][posY].getCouleur()==Case::Couleur::BLANCHE && grilleInt[posX][posY]<0){
			chercherNumeroGroupement(grille[posX][posY],grilleInt);
		}
	}

	posX=x;
	posY=y+1;

	if(estDansGrille(posX,posY)){
		if(grille[posX][posY].getCouleur()==Case::Couleur::BLANCHE && grilleInt[posX][posY]<0){
			chercherNumeroGroupement(grille[posX][posY],grilleInt);
		}
	}

	posX=x;
	posY=y-1;

	if(estDansGrille(posX,posY)){
		if(grille[posX][posY].getCouleur()==Case::Couleur::BLANCHE && grilleInt[posX][posY]<0){
			chercherNumeroGroupement(grille[posX][posY],grilleInt);
		}
	}
}

int Grille::getNumeroGroupement(int x, int y){
	int numeroGroupement=-1;
	if(grille[x][y].getCoordCaseGroupement().getX()==-1){
		return 1000;
	}
	if(grille[x][y].getCouleur()==Case::Couleur::BLANCHE)
	{
		//allocation mémoire
		int ** grilleInt=new int*[ligne];
		for(int i=0;i<ligne;i++){
			grilleInt[i]=new int[colonne];
		}

		//initialisation
		for(int i=0;i<ligne;i++){
			for(int j =0;j<colonne;j++){
				grilleInt[i][j]=-1;
			}
		}

		//collecte d'information
		chercherNumeroGroupement(grille[x][y],grilleInt);

		//récupération d'informations (boucle while pour changer)
		int indiceLigne=0,indiceColonne=0;
		while(indiceLigne<ligne && numeroGroupement==-1){
			while(indiceColonne<colonne && numeroGroupement==-1){
				if(grilleInt[indiceLigne][indiceColonne]>0){
					numeroGroupement=grilleInt[indiceLigne][indiceColonne];
				}
				indiceColonne++;
			}
			indiceLigne++;
			indiceColonne=0;
		}

		//libération de la mémoire
		for(int i=0;i<ligne;i++){
			delete[] grilleInt[i];
		}
		delete[] grilleInt;
	}
	return numeroGroupement;

}

bool Grille::groupementFini(int x,int y){
	return (getNumeroGroupement(x,y)==nbrCaseGroupement(x,y));
}


void Grille::contourNoir(int x , int y){

		bool ** grilleBool=new bool*[ligne];
		for(int i=0;i<ligne;i++){
			grilleBool[i]=new bool[colonne];
		}

		for(int i=0;i<ligne;i++){
			for(int j=0;j<colonne;j++){
				grilleBool[i][j]=false;
			}
		}

				bool ** grilleBool2=new bool*[ligne];
		for(int i=0;i<ligne;i++){
			grilleBool2[i]=new bool[colonne];
		}

		for(int i=0;i<ligne;i++){
			for(int j=0;j<colonne;j++){
				grilleBool2[i][j]=false;
			}
		}


		marquerCaseGroupement(grille[x][y],grilleBool);
		marquerCasesContour(grilleBool,grilleBool2);


	for (int x = 0; x< ligne; x++ ){
		for (int y =0;y < colonne; y++){
			if(grilleBool2[x][y] && !estNoire(x,y)) devientNoire(x,y);
		}
	}




	for(int i=0;i<ligne;i++){
			delete[] grilleBool[i];
	}
		delete[] grilleBool;


	for(int i=0;i<ligne;i++){
			delete[] grilleBool2[i];
	}
		delete[] grilleBool2;
}

void Grille::marquerCasesContour(bool ** grilleTest,bool ** grilleMarque){



	for (int x = 0; x< ligne; x++ ){
		for (int y =0;y < colonne; y++){
			if (grilleTest[x][y]){
				if (estDansGrille(x+1,y) && !grilleTest[x+1][y])
					grilleMarque[x+1][y] = true;
				if (estDansGrille(x-1,y) && !grilleTest[x-1][y])
					grilleMarque[x-1][y] = true;
				if (estDansGrille(x,y+1) && !grilleTest[x][y+1])
					grilleMarque[x][y+1] = true;
				if (estDansGrille(x,y-1) && !grilleTest[x][y-1])
					grilleMarque[x][y-1] = true;
			}

		}
	}
}

void Grille::afficheGrilleBoolDEBUG (bool ** grilleBool){
	for (int x = 0; x< ligne; x++ ){
		for (int y =0;y < colonne; y++){
			if (grilleBool[x][y]) cout << " T ";
			else cout << " F ";
		}

	cout << endl;
	}
	cout << endl <<endl;
}

void Grille::testGeneraux(int x,int y){
	//if(!lever && estNoire(9,5)){cout<<"x: "<<x<<" , y: "<<y<<endl;lever=true;}

	if(estNoire(x,y))etendreLigneNoire(grille[x][y]);

	if (estGrise(x,y)) carreNoir(x,y);

	if (estGrise(x,y)) isolementCaseNoire(x,y);
	if (estBlanche(x,y) && !groupementFini(x,y)) isolementCaseBlanche(x,y);
	//if(!lever && estNoire(9,5)){cout<<"x: "<<x<<" , y: "<<y<<endl;lever=true;}
}

int Grille::nbrCaseGrise(){
	int nbrCase = 0;
	for(int i=0;i<ligne;i++){
			for(int j=0;j<colonne;j++){
				if (estGrise(i,j)) nbrCase++;
			}
		}
	return nbrCase;
}

int Grille::nbrCaseBlanche(){
	int nbrCase = 0;
	for(int i=0;i<ligne;i++){
			for(int j=0;j<colonne;j++){
				if (estBlanche(i,j)) nbrCase++;
			}
		}
	return nbrCase;
}

int Grille::nbrCaseNoire(){
	int nbrCase = 0;
	for(int i=0;i<ligne;i++){
			for(int j=0;j<colonne;j++){
				if (estNoire(i,j)) nbrCase++;
			}
		}
	return nbrCase;

}

void Grille::etendreGroupement(Case& caseBlanche){
	bool ** grilleBool=new bool*[ligne];
	for(int i=0;i<ligne;i++){
		grilleBool[i]=new bool[colonne];
	}

	for(int i=0;i<ligne;i++){
		for(int j=0;j<colonne;j++){
			grilleBool[i][j]=false;
		}
	}

	marquerCasesPotentiellesGroupement(grilleBool,caseBlanche);

	int compteurCasePossibles=0;
	int coordCaseX=0;
	int coordCaseY=0;
	for(int i=0;i<ligne;i++){
		for(int j=0;j<colonne;j++){
			if(grilleBool[i][j] && estGrise(i,j)){
				//cout<<"coord potentielles :"<<i<<" "<<j<<endl;
				compteurCasePossibles++;
				coordCaseX=i;
				coordCaseY=j;
			}
		}
	}
	if(compteurCasePossibles==1){
		devientBlanche(coordCaseX,coordCaseY);
	}
	for(int i=0;i<ligne;i++){
		delete[] grilleBool[i];
	}
	delete[] grilleBool;
}

void Grille::marquerCasesPotentiellesGroupement(bool** grilleBool,Case& caseBlanche){
	int x=caseBlanche.getCoordX();
	int y=caseBlanche.getCoordY();
	grilleBool[x][y]=true;
	chercheGroupement(x,y);

	if(estDansGrille(x+1,y) && !grilleBool[x+1][y]){
		if(estBlanche(x+1,y)){
			marquerCasesPotentiellesGroupement(grilleBool, grille[x+1][y]);
		}
		else if(estGrise(x+1,y)){
			grilleBool[x+1][y]=true;
		}
	}
	if(estDansGrille(x-1,y) && !grilleBool[x-1][y]){
		if(estBlanche(x-1,y)){
			marquerCasesPotentiellesGroupement(grilleBool,grille[x-1][y]);
		}
		else if(estGrise(x-1,y)){
			grilleBool[x-1][y]=true;
		}
	}

	if(estDansGrille(x,y+1) && !grilleBool[x][y+1]){
		if(estBlanche(x,y+1)){
			marquerCasesPotentiellesGroupement(grilleBool,grille[x][y+1]);
		}
		else if(estGrise(x,y+1)){
			grilleBool[x][y+1]=true;
		}
	}

	if(estDansGrille(x,y-1) && !grilleBool[x][y-1]){
		if(estBlanche(x,y-1)){
			marquerCasesPotentiellesGroupement(grilleBool,grille[x][y-1]);
		}
		else if(estGrise(x,y-1)){
			grilleBool[x][y-1]=true;
		}
	}
}
Case& Grille::getCaseGrille(int x,int y){
	if(estDansGrille(x,y))
		return grille[x][y];
	else
		return grille[0][0];
}

/*
bool Grille::resoudreGrille(Grille grille){

	if(estFini(grille))
		return true;
	else {
		Coord C = grille.nextCaseTest();
	    Grille	grilleDuplique = duplique(grille);
		grille.devenirBlanche(C);
		if (grille.estFonctionnellle()) {
				resoudreGrille(grille);
				return true;
		}
		else {
		grille = grilleDuplique;
		grille.devenirNoire(C);
		if (grille.estFonctiennelle()){
						resoudreGrille(grille);
				return true;
		}

		return grille.estFonctionnelle();
	}


}*/


bool Grille::ligneIsolee(Case& caseNoire){
	bool ** grilleBool=new bool*[ligne];
	for(int i=0;i<ligne;i++){
		grilleBool[i]=new bool[colonne];
	}

	for(int i=0;i<ligne;i++){
		for(int j=0;j<colonne;j++){
			grilleBool[i][j]=false;
		}
	}

	marquerCasesPotentielles(grilleBool,caseNoire);

	int compteurCasePossibles=0;
	int nombreCaseLigne=0;

	for(int i=0;i<ligne;i++){
		for(int j=0;j<colonne;j++){
			if(grilleBool[i][j] && estGrise(i,j)){
				compteurCasePossibles++;
			}
			else if(estNoire(i,j)){
				nombreCaseLigne++;
			}
		}
	}
	for(int i=0;i<ligne;i++){
		delete[] grilleBool[i];
	}
	delete[] grilleBool;

	return (nombreCaseLigne!=tailleFinaleCaseNoire) && (compteurCasePossibles==0);
}

bool Grille::groupementIsole(Case& caseBlanche){
	bool ** grilleBool=new bool*[ligne];

	for(int i=0;i<ligne;i++){
		grilleBool[i]=new bool[colonne];
	}

	for(int i=0;i<ligne;i++){
		for(int j=0;j<colonne;j++){
			grilleBool[i][j]=false;
		}
	}

	marquerCasesPotentiellesGroupement(grilleBool,caseBlanche);

	int compteurCasePossibles=0;

	for(int i=0;i<ligne;i++){
		for(int j=0;j<colonne;j++){
			if(grilleBool[i][j] && estGrise(i,j)){
				compteurCasePossibles++;
			}
		}
	}

	for(int i=0;i<ligne;i++){
		delete[] grilleBool[i];
	}

	delete[] grilleBool;

	return(caseBlanche.getCoordCaseGroupement().getX() == -1) && (compteurCasePossibles == 0);
}
bool Grille::viableCarreNoir(int i,int j){
	bool viable=true;
	if(estNoire(i,j)){


		if(estDansGrille(i-1,j) && estNoire(i-1,j)){
			if(estDansGrille(i-1,j-1) && estNoire(i-1,j-1)){
				if(estDansGrille(i,j-1) && estNoire(i,j-1)){
					viable=false;
				}
			}
		}
			//zone 2
		if(estDansGrille(i+1,j) && estNoire(i+1,j)){
			if(estDansGrille(i,j-1) && estNoire(i,j-1)){
				if(estDansGrille(i+1,j-1) && estNoire(i+1,j-1)){
					viable=false;
				}
			}
		}
			// 3
		if(estDansGrille(i,j+1) && estNoire(i,j+1)){
			if(estDansGrille(i+1,j) && estNoire(i+1,j)){
				if(estDansGrille(i+1,j+1) && estNoire(i+1,j+1)){
					viable=false;
				}
			}
		}
			// quatre
		if(estDansGrille(i,j+1) && estNoire(i,j+1)){
			if(estDansGrille(i-1,j) && estNoire(i-1,j)){
				if(estDansGrille(i-1,j+1) && estNoire(i-1,j+1)){
					viable=false;
				}
			}
		}
	}
	return viable;
}
bool Grille::estViable(){
	bool viable=true;
	int i=0;
	int j=0;
	if(nbrCaseNoire()>tailleFinaleCaseNoire){
		viable=false;
		//cout<<"oui"<<endl;
	}
	while(viable && i<ligne){
		while(viable && j<colonne){
			//nbrCaseGroupement(int x,int y)
//getNumeroGroupement(int x, int y)
			if(estBlanche(i,j) && grille[i][j].getNumero()!=1){
				if(groupementIsole(grille[i][j])){
					//cout<<11<<endl;
					viable=false;
				}
				if(nbrCaseGroupement(i,j)>getNumeroGroupement(i,j)){ //&& nbrCaseGroupement(i,j)>1){
					//cout<<22<<endl;
					viable=false;
				}
				/*if(!nonFinie() && nbrCaseGroupement(i,j)<getNumeroGroupement(i,j)){
					viable=false;
				}*/
			}
			else if(estNoire(i,j)){

				if(ligneIsolee(grille[i][j])){
				//cout<<33<<endl;
					viable=false;
				}
				if(!viableCarreNoir(i,j)){
					viable=false;
				}
			}

			j++;
		}
		i++;
		j=0;
	}
	return viable;
}
int Grille::nbrCaseLigne(int i,int j){
	int retour=0; // compte le nbr de case composant la ligne
	if(estNoire(i,j)){
		bool** grilleBool=new bool*[ligne];
		for(int i=0;i<ligne;i++){
			grilleBool[i]=new bool[colonne];
		}
		for(int i=0;i<ligne;i++){
			for(int j=0;j<colonne;j++){
				grilleBool[i][j]=false;
			}
		}
		marquerCasesPotentielles(grilleBool,grille[i][j]);

		for(int i=0;i<ligne;i++){
			for(int j=0;j<colonne;j++){
				if(grilleBool[i][j] && estNoire(i,j)){
					retour++;
				}
			}
		}
		for(int i=0;i<ligne;i++){
			delete[] grilleBool[i];
		}
		delete grilleBool;
	}
	return retour;
}
bool Grille::viableConnexite(){
	bool viable=true;
	int i=0;
	int j=0;
	while(viable && i<ligne){
		while(viable && j<colonne){
			if(estNoire(i,j)){
				if(nbrCaseLigne(i,j)!=tailleFinaleCaseNoire){
					viable=false;
				}
			}
			j++;
		}
		j=0;
		i++;
	}
	return viable;
}

Coord Grille::chercherCaseLibreBT(int x,int y){
	Coord retour;
	bool caseTrouvee=false;
	int i,j;
	if(x==-1){
		i=0;
		j=0;
	}
	else if(x>=ligne){
		caseTrouvee=true;//pour pas passer dans les boucles
	}
	else{
		i=x;
		j=y+1;
	}
	while(!caseTrouvee && i<ligne){
		while(!caseTrouvee && j<colonne){
			if(estGrise(i,j))
			{
				retour.setX(i);
				retour.setY(j);
				//cout<<i<<endl;
				caseTrouvee=true;
			}
			j++;
		}
		i++;
		j=0;
	}
	return retour;
}

Case** Grille::copierGrilleCase(Case** grilleCase){
	Case** retour=new Case*[ligne];
	for(int i=0;i<ligne;i++){
		retour[i]=new Case[colonne];
	}

	for(int i=0;i<ligne;i++){
		for(int j=0;j<colonne;j++){
			retour[i][j]=Case(grilleCase[i][j]);
			/*retour[i][j].setNumero(grilleCase[i][j].getNumero());
			retour[i][j].setCouleur(grilleCase[i][j].getCouleur());
			retour[i][j].setGroupement(grilleCase[i][j].getCoordCaseGroupement());
			retour[i][j].setCoordCase(grilleCase[i][j].getCoordCase());*/
		}
	}

	return retour;
}

bool Grille::nonFinie(){
	return nbrCaseGrise()!=0;
}
int compteur=0;
bool Grille::backtrack(){
	compteur++;
	if(compteur>10000){
		return false;
	}
	/*if(!nonFinie()){
		return estViable();
	}*/
	Case** sauvegarde=copierGrilleCase(grille);
	Coord emplacementPossible;
	emplacementPossible=chercherCaseLibreBT(-1,0);
	//cout<<emplacementPossible.getY()<<endl;
	//int compteur=0;
	//cout<<0<<endl;
	while(emplacementPossible.getX()!=-1){
		//cout<<emplacementPossible.getX()<<" , "<<emplacementPossible.getY()<<endl;
		//compteur++;
		devientBlanche(emplacementPossible.getX(),emplacementPossible.getY());
		parcoursV1();
		if(estViable()){
			//parcoursV1();
			if(nonFinie()){
				//cout<<"oui";
				if(backtrack()){
					//cout<<"oui"<<endl;
					return true;
				}
			}
			else{
				if(viableConnexite() && estViable())
					return true;
			}
		}
		grille=copierGrilleCase(sauvegarde);
		devientNoire(emplacementPossible.getX(),emplacementPossible.getY());
		if(viableCarreNoir(emplacementPossible.getX(),emplacementPossible.getY())){
			parcoursV1();
			if(estViable()){
				//cout<<"OUI";
				//parcoursV1();
				if(nonFinie()){
					if(backtrack()){
						return true;
					}
				}
				else{
					if(viableConnexite() && estViable())
						return true;
				}
			}
		}

		grille=copierGrilleCase(sauvegarde);
		emplacementPossible=chercherCaseLibreBT(emplacementPossible.getX(),emplacementPossible.getY());
		//cout<<1<<endl;
	}
	//grille=copierGrilleCase(sauvegarde);
	return false;
}


void Grille::menuTest(){
	int valUser = 0;

	while(valUser != 5){
		cout << "Que souhaitez vous faire sur la grille ?" << endl;
		cout << "1. Premier Parcours"<< endl ; 
		cout << "2. premier Parcours + ParcoursV1"<<endl;
		cout << "3. Parcours + Backtrack"<< endl ; 
		cout << "4. Only Backtrack" << endl;
		cout << "5. Sortie"<< endl ; 
	cin >> valUser;
		switch(valUser){
			case 1: premierParcours();break;
			case 2: premierParcours(); parcoursV1();break; 
			case 3: premierParcours(); parcoursV1(); backtrack(); break;
			case 4: backtrack(); break;
		}

		cout << endl << endl << endl; 
		afficheDEBUG(); 

		cout << "Entrez une valeur pour retourner au menu :"; 
		cin >> valUser;
	}




}

bool Grille::testDebug(){
	int valUser(2);
	cout << endl << "1: Pas a pas  // 2. mode Normal"; 
	cin >> valUser;
	return valUser == 1 ;
}
