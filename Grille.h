#ifndef GRILLE_H
#define GRILLE_H

#include "case.h"
#include <iostream>
#include <string>


class Grille {
private:
	Case** grille;  // ensemble de toutes les cases de la grille
	int ligne;
	int colonne;
	int nbrCasesBlanchesGrille;
	int tailleFinaleCaseNoire;
	bool DEBUG;


public:
//////// Constructeurs
    Grille();
    Grille(Case** grilleCase,int ligne,int colonne);
	Grille(std::string adresse,int 	ligne,int colonne); // constructeur par envoie de grille, appel de chargementGrille() dans "outils-grille"
	Grille(std::string nomFichier);
	Grille(Grille&);
	//~Grille();

/////// Accesseurs Lecture et écriture
	const int getLigne();
	const int getColonne();
	Case ** getGrille();
	Case& getCaseGrille(int x, int y); // renvoit la case coordonnées (x,y)

	int getNbrGrillesDisponibles(std::string nomFichier);
	int getFormatGrille_ligne(std::string,int i);
	int getFormatGrille_colonne(std::string, int i);

	int nbrCaseProchesGrises_V1(int x, int y);

	std::string descripteurGrilles(std::string nomFichier);
	Case** chargerGrilleFichier(std::string nomFichier,int numeroGrille);

///////// Méthodes de modifications de cases, utilisé pour faire appeller des cases alentours. Font la modif puis testGeneraux() sur les alentours. 
    void devientNoire(int x, int y);
    void devientBlanche(int x, int y);
    	void chercheGroupement(int x, int y);

    	void testGeneraux(int x,int y); // Teste les cases proches d'une case determinée 

///////// Valeurs pour divers tests
    	int nbrCaseGrise();
    	int nbrCaseBlanche();
    	int nbrCaseNoire();

///////// Test booleen sur les cases 
	bool estBlanche(int x, int y);
	bool estNoire(int x, int y);
	bool estGrise(int x, int y);
	bool estDansGrille(int x, int y);


//////Fonctions  de Debug
	void quelCouleurTerminal(int x, int y); // renvoit les coordoonées suivi de la couleur de la case dans le terminal
	void afficheDEBUG();
	void afficheGrilleBoolDEBUG (bool ** grilleBool); // affiche une grille de bool pour verifier qu'elle fonctionne

////Méthodes de résolution
	////Parcours des cas de Départ 
		void premierParcours();  
			void casUn(int x, int y);
			void rechercheCaseNonAtteignable(int x, int y, int d, bool** grilleBool);
			void rechercheEntreDeux(int x, int y);

	/////Parcours généraux
		void parcoursV1();	
			//Sur Case Grise
				void isolementCaseNoire(int x, int y); 
					bool entoureParBlancOuVide(int x , int y);
				void carreNoir(int x, int y);
			//Sur Case Noire
				void etendreLigneNoire(Case& caseNoire);	
					void marquerCasesPotentielles(bool ** grille,Case& c);
			//Sur Case Blanche
				void isolementCaseBlanche(int x, int y); 
					bool entoureParNoirOuVide(int x , int y);
				void etendreGroupement(Case& c);
					void marquerCasesPotentiellesGroupement(bool ** grilleBool,Case& caseBlanche);


//Méthode liés à Qt
	int nbChiffre(int n);
	int getChiffreNombre(int n,int i);
	int getTailleImpression();
	void imprimerGrille(char const * nomFichier);

//Méthodes expérimentales
	int nbrCaseGroupement(int x, int y);
		void marquerCaseGroupement(Case c,bool ** grilleBool);

	int getNumeroGroupement(int x, int y);
		void chercherNumeroGroupement(Case c,int ** grilleInt);
		
			bool groupementFini(int x,int y);


		void contourNoir(int xCaseBlanche,int yCaseBlanche); // Noirci le contour d'un groupement -- Utilise marquerCaseGroupement()
			void marquerCasesContour(bool ** grilleBool,bool ** grilleBool2);


//Menu et debug
void menuTest();
bool testDebug();

//////Fonctions nouvelles a travailler pour le backtracking
		//true si la ligne est isolée sans pouvoir s'étendre
		bool ligneIsolee(Case& caseNoire); 
		//true si la case blanche est isolée sans pouvoir se connecter à un groupement
		bool groupementIsole(Case& caseBlanche);
		bool estFini(Case** grille); 
		bool estViable();
		bool resoudreGrille(Case** grille);
		Coord chercherCaseLibreBT(int x,int y);
		Case **copierGrilleCase(Case ** grille);
		bool nonFinie();
		bool viableCarreNoir(int i, int j);
		int nbrCaseLigne(int i,int j);
		bool viableConnexite();
		//BOSS FINAL
		bool backtrack();
};

#endif
