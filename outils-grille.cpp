#include <iostream>
#include <string>
#include <fstream>
#include "outils-grille.h"
using namespace std;

//fonction temporaire de test
void afficheGrille(int ** grille,int nbrColonne,int nbrLigne){
    for(int i=0;i<nbrLigne;i++)
    {
        std::cout<<"|";
        for(int j=0;j<nbrColonne;j++){
                std::cout<<grille[i][j]<<"|";
        }
        std::cout<<std::endl;
    }
}


int getNbrColonneGrille(char * nomFichier){
	int nbrColonne=0;
	ifstream fichier;
	fichier.open(nomFichier);
	bool readingInteger=false;

	char lecteurFichier;
	do{
		lecteurFichier=fichier.get();
		if(lecteurFichier!='N' && lecteurFichier!='B'){
			if(!readingInteger){
				readingInteger=true;
			}
		}
		else{
			readingInteger=false;
		}
		if(!readingInteger && lecteurFichier!=':')
			nbrColonne++;
	}while(lecteurFichier!=EOF && lecteurFichier!=':');
	fichier.close();
	return nbrColonne;
}

int getNbrLigneGrille(char * nomFichier){
	int nbrLigne=1;
	ifstream fichier;
	fichier.open(nomFichier);

	char lecteurFichier;
	do{
		lecteurFichier=fichier.get();
		if(lecteurFichier==':')
			nbrLigne++;

	}while(lecteurFichier!=EOF);
	fichier.close();
	return nbrLigne;
}


int ** chargementGrille(char * nomFichier){
    int nbrLigne=getNbrLigneGrille(nomFichier);
    int nbrColonne=getNbrColonneGrille(nomFichier);
    std::ifstream fichier;
    fichier.open(nomFichier);
    int ** grille=new int*[nbrLigne];
    for(int i=0;i<nbrLigne;i++)
    {
        grille[i]=new int[nbrColonne];
    }


    /**
    * Fonctionnement : Si le caractère lu est un nombre, on ne le marque pas tout de suite dans la grille
    * car il pourrait y avoir le nombre 13, qui prend 2 caractères. Une fois qu'on tombe sur autre chose
    * qu'un nombre, on remet la chaine nombre = la chaîne vide.
    * Si on tombe sur N ou B, on inscrit ça dans la grille
    */
    int indiceLigne=0;
    int indiceColonne=0;
    std::string nombre="";
    char lecteurFichier;
    do{
        lecteurFichier=fichier.get();
        if(lecteurFichier!=EOF)
        {
            if(lecteurFichier!= ':')
            {
                if(lecteurFichier== 'N' || lecteurFichier=='B')
                {

                    if(nombre!="")
                    { // nombre != de la chaine vide <=> il y a un nombre en 'stand by' à marquer dans la grille
                        grille[indiceLigne][indiceColonne]=std::atoi(nombre.c_str());
                        nombre="";
                        indiceColonne++;
                    }
                    if(lecteurFichier== 'N')
                    {
                        grille[indiceLigne][indiceColonne]=-1;
                    }
                    else if(lecteurFichier== 'B')
                    {
                        grille[indiceLigne][indiceColonne]=0;
                    }
                    indiceColonne++;
                }
                else{
                    nombre+=lecteurFichier;
                }
            }
            else{
                if(nombre!="")
                {
                    grille[indiceLigne][indiceColonne]=std::atoi(nombre.c_str());
                    nombre="";
                }
                indiceLigne++;
                indiceColonne=0;
            }
        }
    }while(lecteurFichier != EOF);
    fichier.close();
    return grille;
}
