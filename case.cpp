#include <iostream>
#include "case.h"

using namespace std;

//Constructeurs
Case::Case() : numero(-1), color(GRISE){
	groupement = Coord();
	coordCase=Coord();
}

Case::Case(const int num,int x,int y) : numero(num){
	if(numero > 0){
		color = BLANCHE;
		groupement = Coord(x,y);
	}
	else
		color = GRISE;
}

Case::Case(Case& c){
	this->numero=c.numero;
	this->color=c.color;
	this->groupement=Coord(c.groupement);
	this->coordCase=Coord(c.coordCase);
}
//Setters and getters
const int Case::getNumero() const{
	return numero;
}

Case::Couleur Case::getCouleur() const{
	return color;
}



void Case::setCoordCase(int x,int y){
	coordCase.setX(x);
	coordCase.setY(y);
}

void Case::setCoordCaseGroupe(int x,int y){
	groupement.setX(x);
	groupement.setY(y);
}

Coord Case::getCoordCase() {return coordCase;}


void Case::changeCouleur(Case::Couleur c){
	color = c;
}

void Case::changeNumero(int newNumero){
	numero = newNumero;
	color = BLANCHE;
	groupement=Coord(coordCase.getX(),coordCase.getY());
}

int Case::getCoordX(){
	return getCoordCase().getX();
}

int Case::getCoordY(){
	return getCoordCase().getY();
}

Coord Case::getCoordCaseGroupement(){
	return groupement;
}

void Case::setCoordCaseGroupement(Case c){
	groupement.setX(c.getCoordCaseGroupement().getX());
	groupement.setY(c.getCoordCaseGroupement().getY());
}

std::string Case::toString(){
	std::string res="coord de la case : ";
	res+=coordCase.toString();
	res+="\ncoord du groupement : ";
	res+=groupement.toString();
	if(numero>0){
		res+="\nnumero : ";
		res+=std::to_string(numero);
	}
	res+="\ncouleur : ";
	res+=color;
	return res;
}

void Case::setNumero(int i){
	this->numero=i;
}

void Case::setCouleur(Couleur c){
	this->color=c;
}
	
void Case::setGroupement(Coord gr){
	this->groupement.setX(gr.getX());
	this->groupement.setY(gr.getY());
}

void Case::setCoordCase(Coord c){
	this->coordCase.setX(c.getX());
	this->coordCase.setY(c.getY());
}
