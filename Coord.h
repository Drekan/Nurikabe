#ifndef COORD_H
#define COORD_H
#include <string>
class Coord{
 private:
   int x;
   int y;

 public:

  Coord();
  Coord(int,int);
  Coord(Coord&);

  int getX();
  int getY();
  void setX(int);
  void setY(int); 

  bool estVoisin(int,int);
  std::string toString();
};

#endif
