#include <iostream>
#include <string>
#ifndef OUTILS_GRILLE_H
#define OUTILS_GRILLE_H


void afficheGrille(int ** grille,int nbrColonne,int nbrLigne);
//fonctions qui chargent une grille de Nurikabe à partir d'un fichier qui les stocke
//la variable taille contiendra le format de la grille de Nurikabe (10x,5x...)
int getNbrColonneGrille(char * nomFichier);
int getNbrLigneGrille(char * nomFichier);
int ** chargementGrille(char * nomFichier);

#endif
