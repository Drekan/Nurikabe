#include<iostream>
#include "Coord.h"


Coord::Coord(): x(-1), y(-1) {} 
Coord::Coord(int x,int y): x(x), y(y) {}

Coord::Coord(Coord& c){
	this->x=c.x;
	this->y=c.y;
}

int Coord::getX(){return x;}
int Coord::getY(){return y;}

void Coord::setX(int x) {this->x = x;}
void Coord::setY(int y){this->y = y;}

std::string Coord::toString(){
	std::string res="x: ";
	res+=std::to_string(x);
	res+=" | y: ";
	res+=std::to_string(y);
	return res;
}

